from setuptools import setup
setup(
	name="Playlistgenerator",
	version="1.0",
	author="Nore Vincent and Thomas Paul",
	author_email="vincent.nore@bts-malraux.net",
	description="a program witch generate a playlist with parameters",
	long_description=" the program can create a plylist in M3U of XSPF with parameters like artist, kind, album. you can also check te duration",
	license="GPLv3",
	url="https://framagit.org/PPE3_thomas_nore/PPE3_Playlist",
	packages =['Playlistgenerator'],
	install_requires=["lxml","psycopg2"],
	data_files=[('/usr/share/man/man1',['Playlistgenerator/Playlistgenerator.1'])],
	entry_points={
		'console_scripts':[
			'Playlistgenerator = Playlistgenerator.playlist:main'
		]
	},
	
)
