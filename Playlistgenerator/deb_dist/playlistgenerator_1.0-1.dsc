Format: 3.0 (quilt)
Source: playlistgenerator
Binary: python3-playlistgenerator
Architecture: all
Version: 1.0-1
Maintainer: Nore Vincent and Thomas Paul <vincent.nore@bts-malraux.net>
Standards-Version: 3.9.1
Build-Depends: python3-setuptools, python3-all, debhelper (>= 9)
Package-List:
 python3-playlistgenerator deb python optional arch=all
Checksums-Sha1:
 550a29654edf94ea2c9bafc0fe0999c355010bb4 4777 playlistgenerator_1.0.orig.tar.gz
 d2923a732756282e800489885e35d0d48217a244 800 playlistgenerator_1.0-1.debian.tar.xz
Checksums-Sha256:
 cb8073cd617190dd27887d44938f74352ac37b36b56e7363f9a6c6dd77241978 4777 playlistgenerator_1.0.orig.tar.gz
 aa42b107bbc6416a0bc7f1872574f76ee467e7a13a5f3076c63dec2f35c8c0d2 800 playlistgenerator_1.0-1.debian.tar.xz
Files:
 1d613be878d9a8f1b089bb9d93f36c17 4777 playlistgenerator_1.0.orig.tar.gz
 1517861ba346c9a9c758ac7fab8ab387 800 playlistgenerator_1.0-1.debian.tar.xz
