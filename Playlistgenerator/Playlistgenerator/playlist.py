# -*- coding: utf-8 -*-
"""
Created on Tue Sep 27 16:52:49 2016

@author: Vincent NORE and Paul THOMAS
@version: 1.0
"""

"""
    The program create a playlist with the arguments captured by the user """

""" This is the import use in code """
import logging
import psycopg2
import argparse
import sys
import random
from lxml import etree

""" Header for the lxml """
root = etree.Element('playlist')
playlist = etree.SubElement(root, 'track')

""" Header for log """
logger = logging.getLogger("playlist")
logger.setLevel(logging.DEBUG)

""" Header for argparse """
parser = argparse.ArgumentParser()

                                   
def main():        
        """ Specification of the argument "duration" """
        parser.add_argument("-du", "--duration", type=int, default=180,
                help="duration of the playlist you want default 180")
                
        """ Specification of the argument "artist" """        
        parser.add_argument("-ar", "--artist", nargs=2, action='append',
                help="if you want an artist default none you have also to put a pourcentage")
                
        """ Specification of the argument "type" """         
        parser.add_argument("-t", "--type", nargs=2, action='append', 
                help="if you want an type default none you have also to put a pourcentage")
        
        """ Specification of the argument "album" """         
        parser.add_argument("-al", "--album", nargs=2, action='append',
                help="if you want an genre default none you have also to put a pourcentage")  
              
        """ Specification of the argument "M3U" """       
        parser.add_argument("-M3U","--M3U","-m3u",action="store_true",
                help="It's for choose the option of M3U for playlist format")
        
        """ Specification of the argument "XSPF" """             
        parser.add_argument("-XSPF","--XSPF","-xspf",action="store_true",
                help="It's for choose the option of XSPF for playlist format")
                
        args = parser.parse_args()
        
        """ Writing of the variables connections """
        host = "172.16.99.2"
        user = "paul_vincent_user"
        password = "P@ssword"
        dbname = "radio_libre"
        port = "5432"
            
        """ Connection of the database with variables connection """    
        dbcon = psycopg2.connect(host = host, port = port, dbname = dbname, user = user, password = password)
        cursor = dbcon.cursor()
        
        """ Check of the choice of format for the playlist """
        if args.M3U:
            print("you choose M3U")
        elif args.XSPF:
            print("you choose XSPF")
        else:
            print("error no format choose for the playlist add -M3U or -XSPF")
            sys.exit(0)
        
        """ Adding of the variable "sommePourcentage", initialized in 0 """
        print("creation of the playlist with the duration = {} ".format(args.duration))
        sommePourcentage=0
        
        """ Definition of list """
        trackList = []
        
        """ Conversion of duration initially in minutes in second """
        totalPlaylist = args.duration*60
        
        """ Check if there is an artist in the argument "artist" """
        if args.artist!=None:
         i=0
         resultArtist = []
         """ Loop if there are several artists """
         for arg in args.artist:
            """ Use of the request to look for the chosen artist """  
            cursor.execute("set schema 'paul_vincent'; SELECT duration, path FROM \"Track\" WHERE artist = (SELECT id FROM \"Artist\" WHERE name = '" +format(args.artist[i][0])+"')")
            """ Put the result of the request in a variable """
            resultArtist += cursor.fetchall()
            
            """ Adding of the percentage of the argument in the variable "sommePourcentage" """
            args.artist[i][1]= int(args.artist[i][1])
            sommePourcentage=sommePourcentage+args.artist[i][1]
            i=i+1
        
        """ Check if there is an type in the argument "type" """
        if args.type!=None:
          i=0
          resultType = []
          """ Loop if there are several types """
          for arg in args.type:
            """ Use of the request to look for the chosen type """  
            cursor.execute("set schema 'paul_vincent'; SELECT duration, path FROM \"Track\" WHERE type = (SELECT id FROM \"Type\" WHERE entitled = '" +format(args.type[i][0])+"')")
            """ Put the result of the request in a variable """
            resultType += cursor.fetchall()           
            
            """ Adding of the percentage of the argument in the variable "sommePourcentage" """
            args.type[i][1]=int(args.type[i][1])
            sommePourcentage=sommePourcentage+args.type[i][1]
            i=i+1
        
        """ Check if there is an type in the argument "album" """
        if args.album!=None:
          i=0
          resultAlbum = []
          """ Loop if there are several albums """
          for arg in args.album:
            """ Use of the request to look for the chosen album """    
            cursor.execute("set schema 'paul_vincent'; SELECT duration, path FROM \"Track\" WHERE album = (SELECT id FROM \"Album\" WHERE title = '" +format(args.album[i][0])+"')")
            """ Put the result of the request in a variable """
            resultAlbum += cursor.fetchall()
                
            """ Adding of the percentage of the argument in the variable "sommePourcentage" """    
            args.album[i][1]=int(args.album[i][1])
            sommePourcentage=sommePourcentage+args.album[i][1]
            i=i+1
            
        """ Check if there is an artist in the argument "artist" """    
        if args.artist!=None:
            i=0
            """ Loop if there are several artists """
            for arg in args.artist:
                """ Calculate of the percentage """
                args.artist[i][1]=args.artist[i][1]*(100/sommePourcentage)
                print("for the artist \"{}\" the percentage is {}".format(args.artist[i][0],args.artist[i][1]) )
                """ Put the percentage of the argument in second """                
                args.artist[i][1]= args.artist[i][1]*totalPlaylist/100
                """ Adding of the variable "totalartist", initialized in 0 """
                totalartist=0
                """ Disturbed by the results of the variable "resultArtist" """
                random.shuffle(resultArtist)
                
                """ Loop as long as there is several line in the variable "resultArtist" """
                for track in resultArtist:
                  """ Check the duration of the music is lower than the total duration """  
                  if (track[0]+totalartist)<args.artist[i][1]:
                     """ Adding the track in the list "trackList" """ 
                     trackList.append(track[1])
                     """ Adding the duration of the track who match with the track + the variable "totalartist" in the variable "totalartist" """
                     totalartist=totalartist+track[0]                     
                i=i+1
                
        """ Check if there is an type in the argument "type" """ 
        if args.type!=None:
            i=0
            """ Loop if there are several types """
            for arg in args.type:
                """ Calculate of the percentage """
                args.type[i][1]=args.type[i][1]*(100/sommePourcentage)
                print("now for the type \"{}\" the percentage is {}".format(args.type[i][0],args.type[i][1]))
                """ Put the percentage of the argument in second """
                args.type[i][1]= args.type[i][1]*totalPlaylist/100
                """ Adding of the variable "totaltype", initialized in 0 """
                totaltype=0
                """ Disturbed by the results of the variable "resultType" """
                random.shuffle(resultType)
                
                """ Loop as long as there is several line in the variable "resultType" """
                for type in resultType:
                  """ Check the duration of the music is lower than the total duration """  
                  if (type[0]+totaltype)<args.type[i][1]:
                     """ Adding the type in the list "trackList" """  
                     trackList.append(type[1])
                     """ Adding the duration of the track who match with the type + the variable "totalartist" in the variable "totalartist" """
                     totaltype=totaltype+type[0]    
                i=i+1 
                
        """ Check if there is an album in the argument "album" """        
        if args.album!=None:
            i=0
            """ Loop if there are several albums """
            for arg in args.album:
                """ Calculate of the percentage """
                args.album[i][1]=args.album[i][1]*(100/sommePourcentage)
                print("now for the album \"{}\" the percentage is {}".format(args.album[i][0],args.album[i][1]))
                """ Put the percentage of the argument in second """
                args.album[i][1]= args.album[i][1]*totalPlaylist/100
                """ Adding of the variable "totalalbum", initialized in 0 """
                totalalbum=0
                """ Disturbed by the results of the variable "resultAlbum" """
                random.shuffle(resultAlbum)
                
                """ Loop as long as there is several line in the variable "resultAlbum" """
                for album in resultAlbum:
                  """ Check the duration of the music is lower than the total duration """  
                  if (album[0]+totalalbum)<(args.album[i][1]+30):
                     """ Adding the album in the list "trackList" """  
                     trackList.append(album[1])
                     """ Adding the duration of the track who match with the album + the variable "totalartist" in the variable "totalartist" """
                     totalalbum=totalalbum+album[0]   
                i=i+1
        
        """ Check if there is an argument "XSPF" chosen 
            If there is an argument "XSPF" it goes into the if
        """        
        if args.XSPF:
            """ Loop as long as there is several line in the list "trackList" """
            for track in trackList:
                   """ Définition of the parent node """
                   location = etree.SubElement(playlist, 'location')
                   """ Display of the line in the list """
                   location.text = ("\n" + track + "\n")
            try:
                with open('playlist.xspf', 'w') as fic:
                    fic.write(etree.tostring(root, pretty_print=True).decode('utf-8'))
            except IOError:
                print('Problème rencontré lors de l\'écriture...')
                exit(1)
            """ Otherwise, by default it chosen the format "M3U" """      
        else:
            """ Open the file
                if it doesn't exist, it create it
                WARNING, if the filz already exist, it wil be crushed
            """
            fichier = open("playlist.m3u", "w")
            """ Loop as long as there is several line in the list "trackList" """
            for track in trackList:
                """ Display of the line in the list """
                fichier.write("\n" + track)
            """ Close the writing in the file """    
            fichier.close()
                   
        cursor.close()
        dbcon.commit()
        """ Close the connection """
        dbcon.close()      
                
     
""" Check if the module "playlist" run or if it loads """  
if __name__ == "__main__":
    logger.debug("Execution of the module 'playlist'")
    main()
    logger.debug("End of the execution of the module 'playlist'")
else:
    logger.debug("Loding of the module 'playlist'")
    
    